package summative.five.backend.controllers;






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import summative.five.backend.models.Animal;
import summative.five.backend.services.AnimalService;



@RestController
@RequestMapping("/animals")
@Validated
public class AnimalController {

    @Autowired
    AnimalService service;



    @GetMapping
    public Iterable<Animal> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Animal save(@RequestBody Animal user) {
        return service.save(user);
    }



    @GetMapping("/{id}")
    public Animal getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Animal updateById(@PathVariable int id, @RequestBody Animal user) {
        return service.updateById(id, user);
    }

}