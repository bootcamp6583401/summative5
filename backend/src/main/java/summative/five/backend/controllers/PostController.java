package summative.five.backend.controllers;






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import summative.five.backend.models.Post;
import summative.five.backend.services.PostService;



@RestController
@RequestMapping("/posts")
@Validated
public class PostController {

    @Autowired
    PostService service;



    @GetMapping
    public Iterable<Post> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Post save(@RequestBody Post user) {
        return service.save(user);
    }



    @GetMapping("/{id}")
    public Post getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Post updateById(@PathVariable int id, @RequestBody Post user) {
        return service.updateById(id, user);
    }

}