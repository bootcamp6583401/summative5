package summative.five.backend.controllers;






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import summative.five.backend.models.Event;
import summative.five.backend.services.EventService;



@RestController
@RequestMapping("/events")
@Validated
public class ZooEventController {

    @Autowired
    EventService service;



    @GetMapping
    public Iterable<Event> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Event save(@RequestBody Event user) {
        return service.save(user);
    }



    @GetMapping("/{id}")
    public Event getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Event updateById(@PathVariable int id, @RequestBody Event user) {
        return service.updateById(id, user);
    }

}