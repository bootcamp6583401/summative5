package summative.five.backend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import summative.five.backend.models.Animal;





public interface AnimalRepository extends CrudRepository<Animal, Integer> {

    @Query(value ="SELECT * FROM animal", nativeQuery = true)
    List<Animal> findAll();
}
