package summative.five.backend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import summative.five.backend.models.Event;





public interface EventRepository extends CrudRepository<Event, Integer> {

    @Query(value ="SELECT * FROM event", nativeQuery = true)
    List<Event> findAll();
}
