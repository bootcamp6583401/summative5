package summative.five.backend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import summative.five.backend.models.Post;





public interface PostRepository extends CrudRepository<Post, Integer> {

    @Query(value ="SELECT * FROM post", nativeQuery = true)
    List<Post> findAll();
}
