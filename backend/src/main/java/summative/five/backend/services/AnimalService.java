package summative.five.backend.services;


import java.util.List;

import summative.five.backend.models.Animal;





public interface AnimalService {
    List<Animal> getAll();

    Animal getById(int id);

    Animal save(Animal obj);

    void deleteById(int id);

    Animal updateById(int id, Animal brand);



}
