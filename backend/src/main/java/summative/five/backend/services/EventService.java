package summative.five.backend.services;


import java.util.List;

import summative.five.backend.models.Event;





public interface EventService {
    List<Event> getAll();

    Event getById(int id);

    Event save(Event obj);

    void deleteById(int id);

    Event updateById(int id, Event brand);



}
