package summative.five.backend.services;


import java.util.List;

import summative.five.backend.models.Post;





public interface PostService {
    List<Post> getAll();

    Post getById(int id);

    Post save(Post obj);

    void deleteById(int id);

    Post updateById(int id, Post brand);



}
