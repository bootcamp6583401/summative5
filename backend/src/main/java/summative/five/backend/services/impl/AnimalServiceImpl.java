package summative.five.backend.services.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import summative.five.backend.models.Animal;
import summative.five.backend.repositories.AnimalRepository;
import summative.five.backend.services.AnimalService;

import java.util.List;
import java.util.Optional;

@Service
public class AnimalServiceImpl implements AnimalService {

    @Autowired
    AnimalRepository repo;

    public List<Animal> getAll() {
        return repo.findAll();
    }


    public Animal save(Animal obj) {
        return repo.save(obj);

    }

    public Animal getById(int id) {
        Optional<Animal> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Animal updateById(int id, Animal obj) {
        obj.setId(id);
        return repo.save(obj);


    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }


}