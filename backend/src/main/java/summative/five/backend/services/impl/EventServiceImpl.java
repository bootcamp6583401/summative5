package summative.five.backend.services.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import summative.five.backend.models.Event;
import summative.five.backend.repositories.EventRepository;
import summative.five.backend.services.EventService;

import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    EventRepository repo;

    public List<Event> getAll() {
        return repo.findAll();
    }


    public Event save(Event obj) {
        return repo.save(obj);

    }

    public Event getById(int id) {
        Optional<Event> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Event updateById(int id, Event obj) {
        obj.setId(id);
        return repo.save(obj);


    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }


}