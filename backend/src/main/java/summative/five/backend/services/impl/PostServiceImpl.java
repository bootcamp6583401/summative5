package summative.five.backend.services.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import summative.five.backend.models.Post;
import summative.five.backend.repositories.PostRepository;
import summative.five.backend.services.PostService;

import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    PostRepository repo;

    public List<Post> getAll() {
        return repo.findAll();
    }


    public Post save(Post obj) {
        return repo.save(obj);

    }

    public Post getById(int id) {
        Optional<Post> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Post updateById(int id, Post obj) {
        obj.setId(id);
        return repo.save(obj);


    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }


}