package summative.five.backend;




import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import summative.five.backend.controllers.AnimalController;
import summative.five.backend.models.Animal;
import summative.five.backend.services.AnimalService;

import java.util.List;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AnimalController.class)
public class AnimalControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AnimalService service;

    @Test
    public void getAllAnimals_thenReturnJsonArray() throws Exception {
        Animal obj1 = new Animal();
        obj1.setId(1);
        obj1.setName("Animal1");
        obj1.setPictureUrl("url1");

        Animal obj2 = new Animal();
        obj2.setId(2);
        obj2.setName("Animal2");
        obj2.setPictureUrl("url2");




        List<Animal> allAnimals = Arrays.asList(obj1, obj2);

        given(service.getAll()).willReturn(allAnimals);

        mockMvc.perform(get("/animals")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(obj1.getName())))
                .andExpect(jsonPath("$[0].pictureUrl", is(obj1.getPictureUrl())))
                .andExpect(jsonPath("$[1].name", is(obj2.getName())))
                .andExpect(jsonPath("$[1].pictureUrl", is(obj2.getPictureUrl())));
    }

    @Test
    public void getAnimalById_WhenAnimalExists() throws Exception {
        int objId = 1;
        Animal obj = new Animal();
        obj.setId(objId);
        obj.setName("Animal1");

        given(service.getById(objId)).willReturn(obj);

        mockMvc.perform(get("/animals/{id}", objId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(obj.getName())));
    }

    @Test
    public void createAnimal_WhenPostAnimal() throws Exception {
        Animal obj = new Animal();
        obj.setId(1);
        obj.setName("Animal1");

        given(service.save(any(Animal.class))).willReturn(obj);

        mockMvc.perform(post("/animals")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Animal1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(obj.getName())));
    }

    

    @Test
    public void deleteAnimal_WhenAnimalExists() throws Exception {
        int objId = 1;

        doNothing().when(service).deleteById(objId);

        mockMvc.perform(delete("/animals/{id}", objId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateAnimal_WhenPutAnimal() throws Exception {
        int objId = 1;
        Animal obj = new Animal();
        obj.setId(objId);
        obj.setName("Updated Animal");

        given(service.updateById(eq(objId), any(Animal.class))).willReturn(obj);

        mockMvc.perform(put("/animals/{id}", objId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated Animal\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(obj.getName())));
    }
}