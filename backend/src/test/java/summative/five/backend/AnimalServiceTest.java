package summative.five.backend;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import summative.five.backend.models.Animal;
import summative.five.backend.repositories.AnimalRepository;
import summative.five.backend.services.impl.AnimalServiceImpl;



@SpringBootTest
public class AnimalServiceTest {
    @Mock
    private AnimalRepository repo;

    @InjectMocks
    private AnimalServiceImpl service;

    @Test
    public void whenGetAllAnimals_thenReturnAnimalList() {
        Animal obj = new Animal();
        obj.setId(1);
        obj.setName("Test Animal");

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Animal> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals("Test Animal", result.get(0).getName());
    }

    @Test
    public void whenGetAnimalById_thenReturnAnimal() {
        Animal obj = new Animal();
        obj.setId(1);
        obj.setName("Test Animal");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Animal result = service.getById(1);

        assertEquals("Test Animal", result.getName());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));
        Animal result = service.getById(1);
        assertNull(result);
    }

    @Test
    public void whenSaveAnimal_thenAnimalShouldBeSaved() {
        Animal obj = new Animal();
        obj.setId(1);
        obj.setName("Test Animal");

        when(repo.save(any(Animal.class))).thenReturn(obj);

        Animal savedAnimal = service.save(obj);

        assertNotNull(savedAnimal);
        assertEquals(obj.getName(), savedAnimal.getName());
        verify(repo, times(1)).save(obj);
    }



    @Test
    public void whenDeleteAnimal_thenAnimalShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateAnimal_thenAnimalShouldBeUpdated() {
        Animal existingAnimal = new Animal();
        existingAnimal.setId(1);
        existingAnimal.setName("Old Animal Name");

        Animal updated = new Animal();
        updated.setId(1);
        updated.setName("Updated Animal Name");

        when(repo.findById(existingAnimal.getId())).thenReturn(Optional.of(existingAnimal));
        when(repo.save(any(Animal.class))).thenAnswer(i -> i.getArguments()[0]);

        Animal updatedAnimal = service.updateById(existingAnimal.getId(), updated);

        assertNotNull(updatedAnimal);
        assertEquals(updated.getName(), updatedAnimal.getName());
        verify(repo, times(1)).save(updatedAnimal);
    }

}

