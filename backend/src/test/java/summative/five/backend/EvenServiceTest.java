package summative.five.backend;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import summative.five.backend.models.Event;
import summative.five.backend.repositories.EventRepository;
import summative.five.backend.services.impl.EventServiceImpl;



@SpringBootTest
public class EvenServiceTest {
    @Mock
    private EventRepository repo;

    @InjectMocks
    private EventServiceImpl service;

    @Test
    public void whenGetAllEvents_thenReturnEventList() {
        Event obj = new Event();
        obj.setId(1);
        obj.setName("Test Event");

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Event> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals("Test Event", result.get(0).getName());
    }

    @Test
    public void whenGetEventById_thenReturnEvent() {
        Event obj = new Event();
        obj.setId(1);
        obj.setName("Test Event");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Event result = service.getById(1);

        assertEquals("Test Event", result.getName());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));
        Event result = service.getById(1);
        assertNull(result);
    }

    @Test
    public void whenSaveEvent_thenEventShouldBeSaved() {
        Event obj = new Event();
        obj.setId(1);
        obj.setName("Test Event");

        when(repo.save(any(Event.class))).thenReturn(obj);

        Event savedEvent = service.save(obj);

        assertNotNull(savedEvent);
        assertEquals(obj.getName(), savedEvent.getName());
        verify(repo, times(1)).save(obj);
    }



    @Test
    public void whenDeleteEvent_thenEventShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateEvent_thenEventShouldBeUpdated() {
        Event existingEvent = new Event();
        existingEvent.setId(1);
        existingEvent.setName("Old Event Name");

        Event updated = new Event();
        updated.setId(1);
        updated.setName("Updated Event Name");

        when(repo.findById(existingEvent.getId())).thenReturn(Optional.of(existingEvent));
        when(repo.save(any(Event.class))).thenAnswer(i -> i.getArguments()[0]);

        Event updatedEvent = service.updateById(existingEvent.getId(), updated);

        assertNotNull(updatedEvent);
        assertEquals(updated.getName(), updatedEvent.getName());
        verify(repo, times(1)).save(updatedEvent);
    }

}

