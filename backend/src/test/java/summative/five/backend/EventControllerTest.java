package summative.five.backend;




import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import summative.five.backend.controllers.ZooEventController;
import summative.five.backend.models.Event;
import summative.five.backend.services.EventService;

import java.util.List;
import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ZooEventController.class)
public class EventControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EventService service;

    @Test
    public void Events_thenReturnJsonArray() throws Exception {
        Event obj1 = new Event();
        obj1.setId(1);
        obj1.setName("Event1");
        obj1.setDate(LocalDate.now());
        obj1.setDescription("Desc 1");
        obj1.setSpecial(true);

        Event obj2 = new Event();
        obj2.setId(2);
        obj2.setName("Event2");
        obj2.setDate(LocalDate.now());
        obj2.setDescription("Desc 2");
        obj2.setSpecial(false);




        List<Event> Events = Arrays.asList(obj1, obj2);

        given(service.getAll()).willReturn(Events);

        mockMvc.perform(get("/events")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(obj1.getName())))
                .andExpect(jsonPath("$[0].description", is(obj1.getDescription())))
                .andExpect(jsonPath("$[0].special", is(obj1.isSpecial())))
                .andExpect(jsonPath("$[0].date", is(obj1.getDate().toString())))
                .andExpect(jsonPath("$[1].name", is(obj2.getName())))
                .andExpect(jsonPath("$[1].description", is(obj2.getDescription())));
    }

    @Test
    public void EventById_WheneventExists() throws Exception {
        int objId = 1;
        Event obj = new Event();
        obj.setId(objId);
        obj.setName("Event1");

        given(service.getById(objId)).willReturn(obj);

        mockMvc.perform(get("/events/{id}", objId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(obj.getName())));
    }

    @Test
    public void Event_WhenPostevent() throws Exception {
        Event obj = new Event();
        obj.setId(1);
        obj.setName("Event1");

        given(service.save(any(Event.class))).willReturn(obj);

        mockMvc.perform(post("/events")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Event1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(obj.getName())));
    }

    

    @Test
    public void Event_WheneventExists() throws Exception {
        int objId = 1;

        doNothing().when(service).deleteById(objId);

        mockMvc.perform(delete("/events/{id}", objId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void Event_WhenPutevent() throws Exception {
        int objId = 1;
        Event obj = new Event();
        obj.setId(objId);
        obj.setName("Updated Event");

        given(service.updateById(eq(objId), any(Event.class))).willReturn(obj);

        mockMvc.perform(put("/events/{id}", objId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated Event\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(obj.getName())));
    }
}