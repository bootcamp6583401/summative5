package summative.five.backend;




import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import summative.five.backend.controllers.PostController;
import summative.five.backend.models.Post;
import summative.five.backend.services.PostService;

import java.util.List;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PostController.class)
public class PostControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PostService service;

    @Test
    public void getAllPosts_thenReturnJsonArray() throws Exception {
        Post obj1 = new Post();
        obj1.setId(1);
        obj1.setName("Post1");
        obj1.setPictureUrl("url1");

        Post obj2 = new Post();
        obj2.setId(2);
        obj2.setName("Post2");
        obj2.setPictureUrl("url2");




        List<Post> allPosts = Arrays.asList(obj1, obj2);

        given(service.getAll()).willReturn(allPosts);

        mockMvc.perform(get("/posts")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(obj1.getName())))
                .andExpect(jsonPath("$[0].pictureUrl", is(obj1.getPictureUrl())))
                .andExpect(jsonPath("$[1].name", is(obj2.getName())))
                .andExpect(jsonPath("$[1].pictureUrl", is(obj2.getPictureUrl())));
    }

    @Test
    public void getPostById_WhenPostExists() throws Exception {
        int objId = 1;
        Post obj = new Post();
        obj.setId(objId);
        obj.setName("Post1");

        given(service.getById(objId)).willReturn(obj);

        mockMvc.perform(get("/posts/{id}", objId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(obj.getName())));
    }

    @Test
    public void createPost_WhenPostPost() throws Exception {
        Post obj = new Post();
        obj.setId(1);
        obj.setName("Post1");

        given(service.save(any(Post.class))).willReturn(obj);

        mockMvc.perform(post("/posts")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Post1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(obj.getName())));
    }

    

    @Test
    public void deletePost_WhenPostExists() throws Exception {
        int objId = 1;

        doNothing().when(service).deleteById(objId);

        mockMvc.perform(delete("/posts/{id}", objId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updatePost_WhenPutPost() throws Exception {
        int objId = 1;
        Post obj = new Post();
        obj.setId(objId);
        obj.setName("Updated Post");

        given(service.updateById(eq(objId), any(Post.class))).willReturn(obj);

        mockMvc.perform(put("/posts/{id}", objId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated Post\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(obj.getName())));
    }
}