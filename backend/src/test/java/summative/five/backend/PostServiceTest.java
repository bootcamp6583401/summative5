package summative.five.backend;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import summative.five.backend.models.Post;
import summative.five.backend.repositories.PostRepository;
import summative.five.backend.services.impl.PostServiceImpl;



@SpringBootTest
public class PostServiceTest {
    @Mock
    private PostRepository repo;

    @InjectMocks
    private PostServiceImpl service;

    @Test
    public void whenGetAllPosts_thenReturnPostList() {
        Post obj = new Post();
        obj.setId(1);
        obj.setName("Test Post");

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Post> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals("Test Post", result.get(0).getName());
    }

    @Test
    public void whenGetPostById_thenReturnPost() {
        Post obj = new Post();
        obj.setId(1);
        obj.setName("Test Post");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Post result = service.getById(1);

        assertEquals("Test Post", result.getName());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));
        Post result = service.getById(1);
        assertNull(result);
    }

    @Test
    public void whenSavePost_thenPostShouldBeSaved() {
        Post obj = new Post();
        obj.setId(1);
        obj.setName("Test Post");

        when(repo.save(any(Post.class))).thenReturn(obj);

        Post savedPost = service.save(obj);

        assertNotNull(savedPost);
        assertEquals(obj.getName(), savedPost.getName());
        verify(repo, times(1)).save(obj);
    }



    @Test
    public void whenDeletePost_thenPostShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdatePost_thenPostShouldBeUpdated() {
        Post existingPost = new Post();
        existingPost.setId(1);
        existingPost.setName("Old Post Name");

        Post updated = new Post();
        updated.setId(1);
        updated.setName("Updated Post Name");

        when(repo.findById(existingPost.getId())).thenReturn(Optional.of(existingPost));
        when(repo.save(any(Post.class))).thenAnswer(i -> i.getArguments()[0]);

        Post updatedPost = service.updateById(existingPost.getId(), updated);

        assertNotNull(updatedPost);
        assertEquals(updated.getName(), updatedPost.getName());
        verify(repo, times(1)).save(updatedPost);
    }

}

