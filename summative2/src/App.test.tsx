import { screen, waitFor } from "@testing-library/react"
import App from "./App"
import { renderWithProviders } from "./utils/test-utils"
import * as AnimalService from "./services/animalService"
import * as PostService from "./services/postService"
import * as EventService from "./services/eventService"
import { vi } from "vitest"
import { Animal, Event, Post } from "./types"

const mockAnimals: Animal[] = [
  { id: 1, name: "Animal 1", pictureUrl: "/example" },
  { id: 2, name: "Animal 2", pictureUrl: "/example" },
  { id: 3, name: "Animal 3", pictureUrl: "/example" },
  { id: 4, name: "Animal 4", pictureUrl: "/example" },
  { id: 5, name: "Animal 5", pictureUrl: "/example" },
  { id: 6, name: "Animal 6", pictureUrl: "/example" },
  { id: 7, name: "Animal 7", pictureUrl: "/example" },
]

const mockPosts: Post[] = [
  { id: 1, name: "post 1", pictureUrl: "/example" },
  { id: 2, name: "post 2", pictureUrl: "/example" },
  { id: 3, name: "post 3", pictureUrl: "/example" },
  { id: 4, name: "post 4", pictureUrl: "/example" },
  { id: 5, name: "post 5", pictureUrl: "/example" },
  { id: 6, name: "post 6", pictureUrl: "/example" },
  { id: 7, name: "post 7", pictureUrl: "/example" },
]

const mockEvents: Event[] = [
  {
    id: 1,
    name: "event 1",
    description: "description",
    date: "2024-04-04",
    special: true,
  },
  {
    id: 2,
    name: "event 2",
    description: "description",
    date: "2024-04-04",
    special: true,
  },
  {
    id: 3,
    name: "event 3",
    description: "description",
    date: "2024-04-04",
    special: true,
  },
  {
    id: 4,
    name: "event 4",
    description: "description",
    date: "2024-04-04",
    special: false,
  },
  {
    id: 5,
    name: "event 5",
    description: "description",
    date: "2024-04-04",
    special: false,
  },
  {
    id: 6,
    name: "event 6",
    description: "description",
    date: "2024-04-04",
    special: false,
  },
  {
    id: 7,
    name: "event 7",
    description: "description",
    date: "2024-04-04",
    special: false,
  },
]

beforeEach(() => {
  vi.spyOn(AnimalService, "getAnimals").mockImplementation(
    () => new Promise(res => setTimeout(() => res(mockAnimals), 200)),
  )
  vi.spyOn(PostService, "getPosts").mockImplementation(
    () => new Promise(res => setTimeout(() => res(mockPosts), 200)),
  )
  vi.spyOn(EventService, "getEvents").mockImplementation(
    () => new Promise(res => setTimeout(() => res(mockEvents), 200)),
  )
})
afterEach(() => {
  vi.restoreAllMocks()
})

test("App should have correct initial render", async () => {
  renderWithProviders(<App />)

  // app renders
  expect(screen.getByText(/Meet\sOur\sAnimals/i)).toBeInTheDocument()
})

test("Correctly render models (animals, posts, events) ", async () => {
  renderWithProviders(<App />)

  // wait for api fetch
  await waitFor(() => {
    // max animal card display at one time is 7
    expect(screen.getAllByTestId("animal-card").length).toBe(7)

    // max event card displayed at one time is 4
    expect(screen.getAllByTestId("event-card").length <= 4).toBe(true)

    // max post card displayed at one time is 3
    expect(screen.getAllByTestId("post-card").length <= 3).toBe(true)

    // only special events are displayed on moving text banner
    expect(screen.getAllByTestId("event-banner-item").length).toBe(3)
  })
})
