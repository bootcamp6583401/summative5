import "./App.css"
import Footer from "@/components/Footer"
import Header from "@/components/Header"
import logo from "./logo.svg"
import Content from "./components/Content"
import { useAppDispatch, useAppSelector } from "./app/hooks"
import { init, selectAnimals } from "./features/zoo/zooSlice"
import { useEffect } from "react"

const App = () => {
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(init(null))
  }, [])
  return (
    <div className="min-h-screen bg-[url('/images/bg-body.gif')]">
      <div className="mx-auto w-auto bg-[url('/images/bg-page.gif')] bg-no-repeat bg-bottom h-full">
        <Header />
        <Content />
        <Footer />
      </div>
    </div>
  )
}

export default App
