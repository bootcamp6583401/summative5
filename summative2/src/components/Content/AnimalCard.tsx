import { Animal } from "@/types"

export default function AnimalCard({ animal }: { animal: Animal }) {
  return (
    <div data-testid="animal-card">
      <div>
        <img src={animal.pictureUrl} className="w-[98px]" />
      </div>
      <div className="text-center">{animal.name}</div>
    </div>
  )
}
