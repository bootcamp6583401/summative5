import React from "react"
import AnimalCard from "./AnimalCard"
import { Animal } from "@/types"

export default function AnimalSection({ animals }: { animals: Animal[] }) {
  return (
    <section className="h-[191px] mt-[7px] bg-[url('/images/interface.gif')]">
      <div className="pl-8">
        <h1 className="text-[#8E0519] text-[29px] font-bold">
          Meet Our Animals
        </h1>
        <div className="flex gap-4">
          {animals.slice(0, 7).map(a => (
            <AnimalCard animal={a} key={a.id} />
          ))}
          <div>
            <div>
              <img src="/images/button-view-gallery.jpg" />
            </div>
            <div className="text-center">Gallery</div>
          </div>
        </div>
      </div>
    </section>
  )
}
