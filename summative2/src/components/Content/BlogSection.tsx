import { Post } from "@/types"
import React from "react"
import PostCard from "./PostCard"

export default function BlogSection({ posts }: { posts: Post[] }) {
  return (
    <section className="w-[455px]">
      <h2 className="text-[#8E0519] text-xl font-bold">
        Blog: This is just a place holder.
      </h2>
      <p className="text-[#875115] text-[14px] font-bold my-2">
        This is just a place holder, so you can see what the site would look
        like.
      </p>
      <div className="flex">
        <div className="shrink-0">
          <img src="/images/dolphins.jpg" />
        </div>
        <ul className="text-[14px] list-disc pl-6 text-[#875115]">
          <li>
            <p>
              This website template has been designed by Free Website Templates
              for you, for free. You can replace all this text with your own
              text.
            </p>
          </li>
          <li>
            <p>
              Want an easier solution for a Free Website? Head straight to Wix
              and immediately start customizing your website!
            </p>
          </li>
        </ul>
      </div>
      <div className="flex flex-wrap gap-2 w-[540px] mt-3">
        {posts.slice(0, 3).map(p => (
          <PostCard key={p.id} post={p} />
        ))}
      </div>
    </section>
  )
}
