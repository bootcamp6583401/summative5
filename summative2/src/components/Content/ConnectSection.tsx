import React from "react"

export default function ConnectSection() {
  return (
    <section className="w-[225px]">
      <h2 className="text-[#8E0519] text-xl font-bold">Connect</h2>
      <div className="flex flex-col text-[13px] text-[#875316] font-bold gap-[10px]">
        <div className="h-4 pl-[30px] bg-[url('/images/icons.gif')] bg-no-repeat">
          Email
        </div>
        <div
          className="h-4 pl-[30px] bg-[url('/images/icons.gif')] bg-no-repeat"
          style={{
            backgroundPosition: "0 -18px",
          }}
        >
          Facebook
        </div>
        <div
          className="h-4 pl-[30px] bg-[url('/images/icons.gif')] bg-no-repeat"
          style={{
            backgroundPosition: "0 -42px",
          }}
        >
          Twitter
        </div>
      </div>
      <form action="" className="text-[#b00923] mt-4">
        <h3 className="text-[15px]">Subscribe to our</h3>
        <h2 className="text-[20px] font-bold">NEWSLETTER</h2>
        <input type="text" className="text-gray-400 px-2 outline outline-1" />
      </form>
      <div className="">
        <img
          src="/images/penguin2.jpg"
          className="absolute -bottom-12 right-0"
        />
      </div>
    </section>
  )
}
