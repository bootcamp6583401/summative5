import { Event } from "@/types"
import { format } from "date-fns"
import React from "react"

export default function EventCard({ event }: { event: Event }) {
  return (
    <div
      className="py-2 border-b border-dashed border-[#8E0519] last:border-none"
      data-testid="event-card"
    >
      <h3 className="text-[#8E0519] font-semibold">
        {format(new Date(event.date), "MMM dd")}
      </h3>
      <p className="text-[#875316] text-[14px]">{event.description}</p>
    </div>
  )
}
