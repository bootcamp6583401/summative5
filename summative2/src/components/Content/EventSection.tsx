import React from "react"
import EventCard from "./EventCard"
import { Event } from "@/types"

export default function EventSection({ events }: { events: Event[] }) {
  return (
    <section className="w-[225px]">
      <h2 className="text-[#8E0519] text-xl font-bold">Events</h2>
      <div className="flex flex-col">
        {events.slice(0, 4).map(e => (
          <EventCard key={e.id} event={e} />
        ))}
      </div>
    </section>
  )
}
