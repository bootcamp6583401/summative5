import { Post } from "@/types"
import React from "react"

export default function PostCard({ post }: { post: Post }) {
  return (
    <div className="w-[250px] flex gap-2" data-testid="post-card">
      <img
        src={post.pictureUrl}
        alt="gorilla"
        className="w-[140px] block shrink-0 h-[80px] object-cover"
      />
      <div className="text-[#875115] ">
        <div className="font-bold">{post.name}</div>
        <p className="text-[14px]">Post content content content content</p>
      </div>
    </div>
  )
}
