import { useAppSelector } from "@/app/hooks"
import {
  selectAnimals,
  selectEvents,
  selectPosts,
} from "@/features/zoo/zooSlice"
import React from "react"
import AnimalCard from "./AnimalCard"
import EventCard from "./EventCard"
import PostCard from "./PostCard"
import ConnectSection from "./ConnectSection"
import EventSection from "./EventSection"
import BlogSection from "./BlogSection"
import AnimalSection from "./AnimalSection"

export default function Content() {
  const animals = useAppSelector(selectAnimals)
  const events = useAppSelector(selectEvents)
  const posts = useAppSelector(selectPosts)
  return (
    <div className="w-[960px] mx-auto relative">
      <AnimalSection animals={animals} />
      <div className="flex pl-8 gap-2">
        <EventSection events={events} /> <BlogSection posts={posts} />
        <ConnectSection />
      </div>
    </div>
  )
}
