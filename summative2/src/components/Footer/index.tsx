export default function Footer() {
  return (
    <div className="bg-[url('/images/bg-footer.gif')] relative">
      <div className="w-[960px] mx-auto flex text-[#ad8753] text-[14px]">
        <div className="mr-4">
          <img src="/images/animal-kingdom.jpg" />
        </div>
        <div className="flex pt-8 gap-10">
          <div className="flex flex-col gap-2 text-[12px] max-w-[200px]">
            <p>
              This is just a place holder, so you can see what the site would
              look like.
            </p>
            <span>285 067-39 282 / 5282 9273 999</span>
            <span>email@animalkingdomzoo.com</span>
          </div>
          <ul className="">
            <li>
              <a href="index.html">Home</a>
            </li>
            <li>
              <a href="tickets.html">Tickets</a>
            </li>
            <li>
              <a href="zoo.html">The Zoo</a>
            </li>
          </ul>
          <ul className="">
            <li>
              <a href="events.html">Events</a>
            </li>
            <li>
              <a href="blog.html">Blog</a>
            </li>
            <li>
              <a href="gallery.html">Gallery</a>
            </li>
          </ul>
          <ul>
            <li>
              <a href="live.html">Live : Have fun in your visit</a>
            </li>
            <li>
              <a href="live.html">Love : Donate for the animals</a>
            </li>
            <li>
              <a href="live.html">Learn : Get to know the animals</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}
