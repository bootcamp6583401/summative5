import { useAppSelector } from "@/app/hooks"
import { selectEvents } from "@/features/zoo/zooSlice"
import React from "react"

export default function Banner() {
  const events = useAppSelector(selectEvents)
  return (
    <div
      className="w-[915px] h-[66px] mx-auto flex items-center "
      style={{
        backgroundImage: "url(/images/interface.gif)",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "0 -193px",
      }}
    >
      <h1 className="text-[#DFC185] text-[30px]  pl-8 pr-12">
        Special Events:
      </h1>
      <div className="text-[#dadada] overflow-hidden w-[600px] relative h-full">
        <div className="flex gap-[400px] absolute inset-y-0 left-0 items-center weee">
          {events
            .filter(e => e.special)
            .map(e => (
              <div
                className="text-nowrap"
                key={e.id}
                data-testid="event-banner-item"
              >
                {e.name}
              </div>
            ))}
        </div>
      </div>
    </div>
  )
}
