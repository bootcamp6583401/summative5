import React from "react"
import Banner from "./Banner"

export default function Header() {
  return (
    <div className="w-[960px] mx-auto">
      <div className="flex">
        <div>
          <div>
            <img src="/images/logo.jpg" className="" />
          </div>
          <ul className="flex px-4 py-2 ml-4 justify-between border-[#900017] border-t border-b border-dashed">
            <li className="text-[#900017] leading-tight border-r border-[#900017] border-dashed">
              <div className="text-[18px] font-semibold">Live</div>
              <div>Have fun in your visit</div>
            </li>
            <li className="text-[#900017] leading-tight pl-2 border-r border-[#900017] border-dashed">
              <div className="text-[18px] font-semibold">Live</div>
              <div>Have fun in your visit</div>
            </li>
            <li className="text-[#900017] leading-tight pl-2">
              <div className="text-[18px] font-semibold">Live</div>
              <div>Have fun in your visit</div>
            </li>
          </ul>
          <button
            className="block w-[306px] h-[46px] text-white font-bold mx-auto mt-4"
            style={{
              backgroundImage: "url(/images/interface.gif)",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "0 -261px",
            }}
          >
            Buy Tickets / Check Events
          </button>
        </div>

        <div className="w-[602px]">
          <nav>
            <ul className="flex text-white">
              <li className="h-[55px] w-[77px] bg-[url('/images/navigation.gif')] bg-no-repeat text-[#4e311f]">
                <a
                  href="#"
                  className="h-[45px] text-center block leading-[45px]"
                >
                  Home
                </a>
              </li>
              <li className="h-[55px] w-[83px]">
                <a
                  href="#"
                  className="h-[45px] text-center block leading-[45px]"
                >
                  The Zoo
                </a>
              </li>
              <li className="h-[55px] w-[111px]">
                <a
                  href="#"
                  className="h-[45px] text-center block leading-[45px]"
                >
                  Visitors Info
                </a>
              </li>
              <li className="h-[55px] w-[74px]">
                <a
                  href="#"
                  className="h-[45px] text-center block leading-[45px]"
                >
                  Tickets
                </a>
              </li>
              <li className="h-[55px] w-[74px]">
                <a
                  href="#"
                  className="h-[45px] text-center block leading-[45px]"
                >
                  Events
                </a>
              </li>
              <li className="h-[55px] w-[82px]">
                <a
                  href="#"
                  className="h-[45px] text-center block leading-[45px]"
                >
                  Gallery
                </a>
              </li>
              <li className="h-[55px] w-[101px]">
                <a
                  href="#"
                  className="h-[45px] text-center block leading-[45px]"
                >
                  Contact Us
                </a>
              </li>
            </ul>
          </nav>
          <div className="relative">
            <img src="/images/lion-family.jpg" className="relative -left-3" />
          </div>
        </div>
      </div>
      <Banner />
    </div>
  )
}
