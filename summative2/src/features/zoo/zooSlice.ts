import type { PayloadAction } from "@reduxjs/toolkit"
import { createAppSlice } from "../../app/createAppSlice"
import type { AppThunk } from "../../app/store"
import { Animal, Event, Post } from "@/types"
import { getAnimals } from "@/services/animalService"
import { getPosts } from "@/services/postService"
import { getEvents } from "@/services/eventService"

export interface ZooSliceState {
  animals: Animal[]
  posts: Post[]
  events: Event[]
}

const initialState: ZooSliceState = {
  animals: [],
  posts: [],
  events: [],
}

export const zooSlice = createAppSlice({
  name: "zoo",
  initialState,
  reducers: create => ({
    init: create.asyncThunk(
      async () => {
        const animals = await getAnimals()
        const posts = await getPosts()

        const events = await getEvents()

        return {
          animals,
          posts,
          events,
        }
      },
      {
        // pending: state => {
        //   state.status = "loading"
        // },
        fulfilled: (state, action) => {
          state.animals = action.payload.animals
          state.events = action.payload.events
          state.posts = action.payload.posts
        },
      },
    ),
  }),
  selectors: {
    selectAnimals: state => state.animals,
    selectPosts: state => state.posts,
    selectEvents: state => state.events,
  },
})

export const { init } = zooSlice.actions

export const { selectAnimals, selectPosts, selectEvents } = zooSlice.selectors
