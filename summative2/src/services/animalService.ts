/* v8 ignore next 100000 */
// tidak di cover; di mock di App.test.tsx
import axios from "@/lib/axios"
import { Animal } from "@/types"

export const getAnimals = async () => {
  return (await axios.get("/animals")).data as Animal[]
}
