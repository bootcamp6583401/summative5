/* v8 ignore next 100000 */
// tidak di cover; di mock di App.test.tsx
import axios from "@/lib/axios"
import { Event } from "@/types"

export const getEvents = async () => {
  return (await axios.get("/events")).data as Event[]
}
