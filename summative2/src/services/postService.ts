/* v8 ignore next 100000 */
// tidak di cover; di mock di App.test.tsx
import axios from "@/lib/axios"
import { Post } from "@/types"

export const getPosts = async () => {
  return (await axios.get("/posts")).data as Post[]
}
