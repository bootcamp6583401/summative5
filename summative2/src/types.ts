/* v8 ignore next 100000 */

export type Animal = {
  id: number
  name: string
  pictureUrl: string
}

export type Post = {
  id: number
  name: string
  pictureUrl: string
}

export type Event = {
  id: number
  name: string
  description: string
  date: string
  special: boolean
}
